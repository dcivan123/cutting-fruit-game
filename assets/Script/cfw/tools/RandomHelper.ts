
export default class RandomHelper  {


    static random(max:number):number;

    static random(min:number,max:number):number;

    static random(start:number,end?:number):number{
        if(end){
            return Math.floor(Math.random() * (end - start) + start)
        }else{
            return Math.floor( Math.random() * start);
        }
    }

    static randomFloat(min:number,max:number){
        return Math.random() * (max - min) + min
    }

}
