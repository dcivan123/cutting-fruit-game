
export default class PhysicsHelper {


    private static isOpen: boolean = false;
    /**
     * 启用物理引擎
     */
    static open(gravity:cc.Vec2) {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        cc.log('PhysicsHelper  open ')
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().gravity = gravity 
        var pmanager = cc.director.getPhysicsManager();
        // 开启物理步长的设置
        pmanager.enabledAccumulator = true;

        // 物理步长，默认 FIXED_TIME_STEP 是 1/60
        cc.PhysicsManager.FIXED_TIME_STEP = 1 / 30;

        // 每次更新物理系统处理速度的迭代次数，默认为 10
        cc.PhysicsManager.VELOCITY_ITERATIONS = 8;

        // 每次更新物理系统处理位置的迭代次数，默认为 10
        cc.PhysicsManager.POSITION_ITERATIONS = 8;
    }

    static setDebug(flag: boolean) {
        if (this.isOpen) {
            var manager = cc.director.getCollisionManager();
            manager.enabledDebugDraw = flag;
            manager.enabledDrawBoundingBox = flag;
            cc.director.getPhysicsManager().debugDrawFlags =
                cc.PhysicsManager.DrawBits.e_aabbBit |
                cc.PhysicsManager.DrawBits.e_jointBit |
                cc.PhysicsManager.DrawBits.e_shapeBit
                ;      
        }


    }

    /**
     * 关闭物理引擎
     */
    static close() {
        if (!this.isOpen) {
            return;
        }
        this.isOpen = false;
        cc.director.getCollisionManager().enabled = false;
        cc.director.getPhysicsManager().enabled = false;
    }

    static stopRigidBody(ballRigid: cc.RigidBody) {
        ballRigid.gravityScale = 0;
        ballRigid.linearVelocity = cc.v2(0, 0);
        ballRigid.angularVelocity = 0;
    }
}