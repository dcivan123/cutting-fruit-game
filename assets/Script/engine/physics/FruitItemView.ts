


const { ccclass, property } = cc._decorator;

@ccclass
export default class FruitItemView extends cc.Component {

	@property({ type: cc.Sprite, displayName: "icon$VSprite" })
	icon$VSprite: cc.Sprite = null;

	@property
	index: number = 0;

	protected mask: any = null;

	protected rgidBody: cc.RigidBody = null;
	protected collider: cc.PhysicsPolygonCollider = null;

	protected speed: cc.Vec2 = cc.v2(1000, 1000)

	protected rotation: number = 100;


	start() {
		this.init()
	}

	onDestroy() {
		if (this.rgidBody) {
			// this.rgidBody.enabled = false;
			// this.rgidBody.active = false;
			cc.log("FruitItemView  onDestroy ")
			this.rgidBody.active = false;
		}


		if (this.collider) {
			// this.collider.enabled = false;
		}
	}

	setAwake(flag: boolean) {
		this.rgidBody.awake = flag;
	}

	init() {
		if (!this.rgidBody) {
			this.rgidBody = this.node.getComponent(cc.RigidBody)
			this.rgidBody['_init']();
		}
		if (!this.collider) {
			this.collider = this.node.getComponent(cc.PhysicsPolygonCollider)
			this.collider['_init']();
		}
		if (!this.mask) {
			this.mask = this.node.getComponent(cc.Mask)
		}
	}

	getWorldPosition(pos: cc.Vec2) {
		this.rgidBody.getWorldPosition(pos)
	}

	move(angle: number) {
		console.log("点击一次", angle);
		let direction = -angle / 90;
		this.rgidBody.linearVelocity = cc.v2(direction * this.speed.x, this.speed.y);
		this.rgidBody.angularVelocity = direction * this.rotation;
	}

	getLinearVelocity() {
		return this.rgidBody.linearVelocity;
	}

	getAngularVelocity() {
		return this.rgidBody.angularVelocity;
	}

	setGravityScale(scale: number) {
		this.rgidBody.gravityScale = scale;
	}

	getGravityScale() {
		return this.rgidBody.gravityScale
	}

	setLinearVelocity(linearVelocity: cc.Vec2) {
		this.rgidBody.linearVelocity = cc.v2(linearVelocity.x, linearVelocity.y);
	}

	sync() {
		this.rgidBody.syncPosition(true)
		this.rgidBody.syncRotation(true)
	}

	setAngularVelocity(rotation: number) {

		this.rgidBody.angularVelocity = rotation;
	}

	setRigidBodyType(t: cc.RigidBodyType) {

		this.rgidBody.type = t
	}

	setColliderPoints(points: cc.Vec2[]) {

		this.collider.points = points;
		this.collider.apply();
	}

	setMaskPoints(points: cc.Vec2[]) {

		let mg: cc.Graphics = this.mask._graphics;
		if (!mg) {
			return;
		}
		mg.clear();
		// mg.fillColor = cc.color(0, 0, 0)
		for (let index = 0; index < points.length; index++) {
			const to = points[index];

			if (index == 0) {
				mg.moveTo(to.x, to.y);
			} else {
				mg.lineTo(to.x, to.y);
			}
		}
		mg.close()
		mg.fill();
	}



}